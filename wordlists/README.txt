This directory contains the word lists containing the words that
are supposed to be built or guessed.
They are specified specified as comma-separated values (CSV).
Each line has multiple columns for a word in multiple languages.

The columns for the word lists are translations as follows (in that order):

    English, Italian, Spanish, French, German

Every word in the word list MUST be translated in all supported languages.
Everything MUST be written in lowercase (for technical reasons).
The English entry may sometimes be two words, but not more.

Supporting another language would involve translating all the words.

Translation guideline:
Translations should also not use more than two words, unless is really
cannot be avoided. Try to keep translations as short and simple as
possible, but don't oversimplify.
If the English word has multiple meanings, pick the one that seems
to the least far-fetched. If that rule doesn't help, interpret the word
in a way that makes most sense in the Minetest Game universe (that
is, Minetest Game plus mods).

For example, interpret the word "bow" as refering the archery weapon
and not the bowtie because archery is closer related to Minetest Game.

File list:

* 'default_wordlist.csv' is used by default.
* 'animals.csv' is an alternative word list with animals, fantasy creatures
  and related words. Note that default_wordlist.csv may also have such words,
  but easier/simpler ones. This word list is not used by default but can
  be enabled for an arena by using the arena properties.
