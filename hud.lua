quikbild.huds = {}
local S = minetest.get_translator("quikbild")

-- Color for own score
local COLOR_SELF = 0xCFC6B8
-- Color for leader score (needs at least 1 point)
local COLOR_LEADER = 0xF4B41B
-- Color for score of non-leaders in scoreboard
local COLOR_OTHER = 0xA0938E

function quikbild.create_hud(p_name)
    local player = minetest.get_player_by_name(p_name)
    quikbild.huds[p_name] = {}
    quikbild.huds[p_name]["background"] = player:hud_add({

        position = {x=1, y=0},
        alignment = {x=-1, y=1},
        scale = {x = 4, y = 4},
        offset = {x=0, y=0},

        hud_elem_type = "image",
        name = "leaderboard_bg",

        text = "quikbild_hud_bg.png",
    })
    quikbild.huds[p_name]["yourscore"] = player:hud_add({

        position = {x=1, y=0},
        alignment = {x=1, y=1},
        scale = {x = 36*4, y = 3*4},
        offset = {x=-268, y=2*4},

        hud_elem_type = "text",
        name = "yourscore",

        text = "",
        number = COLOR_SELF,
        size = {x=1.5, y=0},
        style = 1,
    })
    for i=1, 5 do
        local id = "leader"..i
        local color
        if i == 1 then
           color = COLOR_OTHER
        end
        quikbild.huds[p_name][id] = player:hud_add({

           position = {x=1, y=0},
           alignment = {x=1, y=1},
           scale = {x = 36*4, y = 3*4},
           offset = {x= -268, y=(6+(i-1)*4)*4},

           hud_elem_type = "text",
           name = id,

           text = "",
           number = 0xF4B41B,
           size = {x=1, y=0},
           style = 1,
       })
    end

end

function quikbild.update_hud(p_name, scoreboard, yourscore)
    local hiscore = scoreboard[1].score

    local player = minetest.get_player_by_name(p_name)

    -- this line is erroring with "attempt to index nil value"
    -- to find the culprit, we will add checks to find out what, exactly is nil.
    if not player then
        minetest.log("error","player is nil!")
    elseif not quikbild.huds then
        minetest.log("error","quikbild.huds is nil")
    elseif not p_name then
        minetest.log("error","p_name is nil")
    elseif not quikbild.huds[p_name] then
        minetest.log("error","quikbild.huds[p_name] is nil")
    elseif not quikbild.huds[p_name]["yourscore"] then
        minetest.log("error",'quikbild.huds[p_name]["yourscore"] is nil')
    elseif not yourscore then
        minetest.log("error","yourscore is nil")
    end

    player:hud_change(quikbild.huds[p_name]["yourscore"], "text", S("Your Score: @1", yourscore))
    for i = 1,5 do
        if scoreboard[i] then
            -- Print score and player name
            local score_line = string.format("%d     %s", scoreboard[i].score, scoreboard[i].name)
            player:hud_change(quikbild.huds[p_name]["leader".. i], "text", score_line)

            -- Highlight the current leader(s)
            local color
            if scoreboard[i].score == hiscore and scoreboard[i].score >= 1 then
                color = COLOR_LEADER
            else
                color = COLOR_OTHER
            end
            player:hud_change(quikbild.huds[p_name]["leader".. i], "number", color)
        else
            player:hud_change(quikbild.huds[p_name]["leader".. i], "text", "")
        end
    end
end

function quikbild.remove_hud(p_name)
    local player = minetest.get_player_by_name(p_name)
    for name,id in pairs(quikbild.huds[p_name]) do
        player:hud_remove(id)
    end
    quikbild.huds[p_name] = nil
end
